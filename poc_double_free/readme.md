# Use After Free in MP4Box.

$ ./bin/gcc/MP4Box -dash 1000 ./crash000131

information reported by sanitizer

```
$ ./bin/gcc/MP4Box -dash 1000 ./crash000131
[iso file] Unknown box type >mEA9E in parent stsd
[iso file] Unknown top-level box type 00009E12
[isom] not enough bytes in box trun: 12 left, reading -939524084 (file isomedia/box_code_base.c, line 7751) - try specifying -no-check (might crash)
[IsoMedia] Track 1 type >mEA9E not natively handled
[Dasher] No template assigned, using $File$_dash$FS$$Number$
[MP4Mux] muxing unknown codec ID Codec Not Supported, using generic sample entry with 4CC ">mEA9E"
[iso file] Unknown box type >mEA9E in parent stsd
[MPD] Generating MPD at time 2023-08-30T07:41:03.018Z
[Dasher] End of Period 
[Dasher] End of MPD (no more active streams)

=================================================================
==644599==ERROR: AddressSanitizer: attempting double-free on 0x602000004d10 in thread T0:
    #0 0x7f5a4e80640f in __interceptor_free ../../../../src/libsanitizer/asan/asan_malloc_linux.cc:122
    #1 0x7f5a4bbdbad3 in gf_filterpacket_del filter_core/filter.c:38
    #2 0x7f5a4bbb1cd7 in gf_fq_del filter_core/filter_queue.c:105
    #3 0x7f5a4bc0b32a in gf_filter_del filter_core/filter.c:664
    #4 0x7f5a4bbca37e in gf_fs_del filter_core/filter_session.c:782
    #5 0x7f5a4b5595f6 in gf_dasher_clean_inputs media_tools/dash_segmenter.c:164
    #6 0x7f5a4b5596b4 in gf_dasher_del media_tools/dash_segmenter.c:173
    #7 0x5575f82f9ede in do_dash /home/functionmain/Desktop/gpac-master-asan/applications/mp4box/mp4box.c:4888
    #8 0x5575f82f9ede in mp4box_main /home/functionmain/Desktop/gpac-master-asan/applications/mp4box/mp4box.c:6239
    #9 0x7f5a4880a082 in __libc_start_main ../csu/libc-start.c:308
    #10 0x5575f82d1f5d in _start (/home/functionmain/Desktop/gpac-master-asan/bin/gcc/MP4Box+0xa5f5d)
```
