# Mypoc

## 介绍
poc of MP4Box.


# rest

## date

20230828

## type

./bin/gcc/MP4Box -dash 1000 -out /dev/null ./now_crashes/dash2/crash1

heap-buffer-overflow
WRITE of size 28416

./bin/gcc/MP4Box -dash 1000 -out /dev/null ./now_crashes/dash2/crash2

runtime error: null pointer passed as argument 1, which is declared to never be null

./bin/gcc/MP4Box -dash 1000 -out /dev/null ./now_crashes/dash2/crash4

heap-buffer-overflow 
READ of size 1769512

./bin/gcc/MP4Box -dash 1000 -out /dev/null ./now_crashes/dash2/crash7

runtime error: division by zero


./bin/gcc/MP4Box -dash 1000 -out /dev/null ./now_crashes/dash2/crash8

runtime error: member access within null pointer of type 'struct GF_DashStream'

