# poc of signed integer overflow

information reported by sanitizer

```
$ ../bin/gcc/MP4Box -dash 1000 ./crash000173
[iso file] default sample description set to 241 but only 1 sample description(s), likely broken ! Fixing to 1
[iso file] default sample description set to 241 but only 1 sample description(s), likely broken ! Fixing to 1
[iso file] TFDT timing 3 higher than cumulated timing 12884901123 (last sample got extended in duration)
[Dasher] No template assigned, using $File$_dash$FS$$Number$
[Dasher] No bitrate property assigned to PID V1, computing from bitstream
filters/mux_isom.c:5716:20: runtime error: signed integer overflow: 25769802246 * 1406331903 cannot be represented in type 'long int'
```



when compile without ASAN:

```
./gpac-master-noasan/bin/gcc/MP4Box -dash 1000 ./crash000173
[iso file] default sample description set to 241 but only 1 sample description(s), likely broken ! Fixing to 1
[iso file] default sample description set to 241 but only 1 sample description(s), likely broken ! Fixing to 1
[iso file] TFDT timing 3 higher than cumulated timing 12884901123 (last sample got extended in duration)
[Dasher] No template assigned, using $File$_dash$FS$$Number$
[Dasher] No bitrate property assigned to PID V1, computing from bitstream
[IsoMedia] File truncated, aborting read for track 115396044.92s 50 %
[MP4Mux] PID A2 ID 2 Sample 2 with DTS 0 less than previous sample DTS 0, patching DTS
[MP4Mux] PID A2 ID 2 Sample 3 with DTS 0 less than previous sample DTS 1, patching DTS
[MPD] Generating MPD at time 2023-08-31T01:51:21.969Z
[Dasher] End of Period 
[Dasher] End of MPD (no more active streams)

free(): double free detected in tcache 2
```



